// fetch('https://jsonplaceholder.typicode.com/todos', {
// 	method: 'GET', 
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// })
// .then((response) => response.json())
// .then((json) => console.log(json))
/*+++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++*/
//PENDING - MAP
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET', 
	headers: {
		'Content-Type': 'application/json'
	},
})	
.then((response) => response.json())
.then((json) => {
	let titles =json.map(item => item.title);
	console.log(titles)
});
/*+++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++*/
//PENDING - SINGLE TO DO LIST ITEM
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET', 
	headers: {
		'Content-Type': 'application/json'
	},
})
.then((response) => response.json())
.then((json) => console.log("The item \"" +json.title+"\" on the list has a status of " +json.completed+ "script."));
/*+++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++*/
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Created to do list item",
		completed: "false",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));
/*+++++++++++++++++++++++++++++++++++++++++++++++++*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1	
	})
})
.then((response) => response.json())
.then((json) => console.log(json));
/*+++++++++++++++++++++++++++++++++++++++++++++++++*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Completed"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*+++++++++++++++++++++++++++++++++++++++++++++++++*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
	});